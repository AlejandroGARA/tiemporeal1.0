<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tiempo Real</title>
    <!-- Reference to the Bing Maps SDK -->
        <script type='text/javascript'
                src='http://www.bing.com/api/maps/mapcontrol?callback=GetMap&key=Ajt0fED4s6v9ZBkormGHEy-LcZIhe0EU1fsJ7mDhS_Wv_7f3VeKVnUhVjj1dFOgh'
                async defer></script>
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  </head>
  <body>





    <?php
    /*
     *---------------------------------------------------------------
     * Guia de conexión API ARUBA
     * UNAMxHacksVI
     * Autor: Laleichin
     * API: https://aruba.easygetpaid.com/api/
     *---------------------------------------------------------------
     *Demo hecho con caracter educativo
     *Ing. Eduardo Alfaro Martínez
    */
    //inicio curl, para manejo de conexiones
    $ch = curl_init();
    //consulto el verbo
    curl_setopt($ch, CURLOPT_URL, "https://aruba.easygetpaid.com/hackathon/tenants/");
    //obtengo transferencia
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //asigno resultado
    $res =json_decode( curl_exec($ch), true);
    //pinto json normal
    // NOTE: echo "<h1>Respuesta JSON</h1>";
    //// NOTE: echo $res;
    //// NOTE: echo "<h1>JSON como objeto</h1>";
    //pinto json bonito
    // NOTE: echo str_replace(array("{", "}", '","'), array("{<br />&nbsp;&nbsp;&nbsp;&nbsp;", "<br />}", '",<br />&nbsp;&nbsp;&nbsp;&nbsp;"'), $res);
    //cierro conexión curl
    curl_close($ch);

    // BUG:
$x = sizeof($res);
for ($i=0; $i < $x; $i++) {
  $a[$i] = json_encode($res[$i]["customer_id"]);
  $b[$i]="";
  for ($k=1; $k < 33; $k++) {
    $b[$i] = $b[$i].$a[$i][$k];
  }
  // NOTE: echo $b[$i],"<br>";
}

function lat($x)
{
  $sc = curl_init();
  curl_setopt($sc,CURLOPT_URL,"https://aruba.easygetpaid.com/hackathon/tenants/$x/sites");
  curl_setopt($sc, CURLOPT_RETURNTRANSFER, true);
  //asigno resultado
  $res =json_decode( curl_exec($sc), true);
  curl_close($sc);
  $la[0] = json_encode($res[0]["latitude"]);
  $b[0]="";
  if($la[0][1] == "-"){$n=11;}else{$n=10;}
  for ($k=1; $k < $n; $k++) {
    $b[0] = $b[0].$la[0][$k];
  }
  return $b[0];

}


function lon($x)
{
  $sc = curl_init();
  curl_setopt($sc,CURLOPT_URL,"https://aruba.easygetpaid.com/hackathon/tenants/$x/sites");
  curl_setopt($sc, CURLOPT_RETURNTRANSFER, true);
  //asigno resultado
  $res =json_decode( curl_exec($sc), true);
  curl_close($sc);
  $lo[0] = json_encode($res[0]["longitude"]);
  $b[0]="";
  if($lo[0][1] == "-"){$n=11;}else{$n=10;}
  for ($k=1; $k < $n; $k++) {
    $b[0] = $b[0].$lo[0][$k];
  }
  return $b[0];
}

$v = sizeof($b);
  for ($i=0; $i < $v; $i++) {
    $latitud[$i] = lat($b[$i]);
    $longitud[$i] = lon($b[$i]);

  }

    ?>

    <!-- <?php  // NOTE: Mapa Google ?>
    <style>
   /* Set the size of the div element that contains the map */
  #map{
      height: 400px;  /* The height is 400 pixels */
      width: 100%;  /* The width is the width of the web page */
    }
 </style>
    <h1>My Google Map</h1>
    <div id="map"></div>
    <script>
      function initMap(){
        var options = {
          zoom:13,
          center:{lat:<?php echo $latitud[1]; ?>,lng:<?php echo $longitud[1]; ?>}
        }

         var marker = new google.maps.Marker({
           position:{lat:<?php echo $latitud[0];?>,lng:<?php echo $longitud[0];?>},
           map:map,
           icon:'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'
         });

      var map = new google.maps.Map(document.getElementById('map'), options)  ;
      }
    </script>
    <script src="http://localhost:35729/livereload.js" charset="utf-8"></script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAktN43RCCFsqHFogo8Oyl0AfAKzRIX_6E&callback=initMap">
    </script> -->

    <ul class="bg-info nav justify-content-end">
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle text-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Perfil</a>
          <div class="dropdown-menu">
            <a class="dropdown-item disabled" href="#">Cuenta</a>
            <a class="dropdown-item disabled" href="#">Añadir IP</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="index.php">Cerrar Sesión</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled text-info" href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
        </li>
    </ul>

    <div id="accordion">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Datos del dispositivo #1
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        <?php echo "Latitud: ",$latitud[0],"    Longitud: ",$longitud[0];?>
        <div id="myMap" style="position:relative;width:600px;height:400px;"></div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Datos del dispositivo #2
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
        <?php echo "Latitud: ",$latitud[1],"    Longitud: ",$longitud[1];?>
        <div id="myMap1" style="position:relative;width:600px;height:400px;"></div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Datos del dispositivo #3
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <?php echo "Latitud: ",$latitud[2],"    Longitud: ",$longitud[2];?>
        <div id="myMap2" style="position:relative;width:600px;height:400px;"></div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
          Datos del dispositivo #4
        </button>
      </h5>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <?php echo "Latitud: ",$latitud[3],"    Longitud: ",$longitud[3];?>
        <div id="myMap3" style="position:relative;width:600px;height:400px;"></div>
      </div>
    </div>
  </div>
</div>










<?php // NOTE: BING 0?>
    <script type='text/javascript'>
    function GetMap()
    {
      var map = new Microsoft.Maps.Map('#myMap', {
        credentials: 'Ajt0fED4s6v9ZBkormGHEy-LcZIhe0EU1fsJ7mDhS_Wv_7f3VeKVnUhVjj1dFOgh',
        center: new Microsoft.Maps.Location(<?php echo $latitud[0]; ?>, <?php echo $longitud[0]; ?>),
        mapTypeId: Microsoft.Maps.MapTypeId.aerial,
        zoom: 15
      });

      var center = map.getCenter();

       //Create custom Pushpin
       var pin = new Microsoft.Maps.Pushpin(center, {
           color: 'red'
       });

       //Add the pushpin to the map
       map.entities.push(pin);

    }
    </script>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>
