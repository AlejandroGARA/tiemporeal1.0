<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- css -->
    <link rel="stylesheet" href="index.css">
    <title>Localizador</title>
  </head>
  <body>
    <div class="d-none d-md-block d-lg-none " style="position:fixed; width:100%; height:100%;">
      <img src="localizar.jpg" style="width:100%; height:100%; position:fixed; z-index:1;">
      <div class="card text-center" style="z-index:2; position:fixed; top:15%; left:20%;right: 20%; bottom:15%;">
        <div class="card-body">
          <h5 class="card-title text-info" style=" height:25%; font-size:400%;">Tiempo real</h5>
          <p class="card-text" style=" height:25%; font-size:200%;position: relative;">Localiza donde se encuentran tus dipositivos en tiempo real</p>
          <div class="" style=" position:relative; top:20%;">
            <a href="session.php" class="btn btn-info" style="font-size:200%;">Inicia sesion</a>
            <a href="registrate.php" class="btn btn-info" style="font-size:200%;">Registrate</a>
          </div>
        </div>
      </div>
    </div>
    <div class="d-none d-lg-block bg-info" style="position:fixed; width:100%; height:100%;">
      <img src="localizar.jpg" style="position:fixed; width: 50%; height:100%; right:0px; border-radius: 50% 0px 0px 50%;">
      <h1 class="text-light" style="position:fixed; top:10%; left: 10%; font-size:500%;">Tiempo real</h1>
      <p class="text-light" style="position:fixed; top:40%; left: 10%; font-size:200%; right:50%">Localiza donde se encuentran tus dipositivos en tiempo real</p>
      <div class="" style=" position:relative; top:80%; left:10%;">
        <a href="session.php" class="btn btn-outline-light" style="font-size:200%;">Inicia sesion</a>
        <a href="registrate.php" class="btn btn-outline-light" style="font-size:200%;">Registrate</a>
      </div>
    </div>






    <!-- Optional JavaScript -->
    <script src="http://localhost:35729/livereload.js" charset="utf-8"></script>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
