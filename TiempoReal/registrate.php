<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html lang="en">
    <head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">

		<!-- Website CSS style -->
		<link rel="stylesheet" type="text/css" href="assets/css/main.css">

		<!-- Website Font style -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

		<!-- Google Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

		<title>Localizador</title>
	</head>
	<body>
		<div class="container">
			<div class="row main">
				<div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h1 class="title text-info">Registrate</h1>
	               		<hr />
	               	</div>
	            </div>
				<div class="main-login main-center">
					<form class="form-horizontal" method="post" action="recibir.php">

						<div class="form-group">
							<label for="name" class="text-info cols-sm-2 control-label">Tu nombre</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="name" id="name"  placeholder="Coloca tu nombre"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="email" class="text-info cols-sm-2 control-label">Tu correo</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="email" id="email"  placeholder="Ingresa tu correo"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="username" class="cols-sm-2 control-label text-info">Nombre de usuario</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="username" id="username"  placeholder="Ingresa tu usuario"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label text-info">Contraseña</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password"  placeholder="Ingresa tu contraseña"/>
								</div>
							</div>
						</div>



						<div class="form-group ">
							<button type="submit" class="btn btn-info btn-lg btn-block login-button">Registrar</button>
						</div>
						<div class="login-register text-info">
				            <a href="session.php" class="text-info">Inicia sesión</a>
				         </div>
					</form>
				</div>
			</div>
		</div>
    <script src="http://localhost:35729/livereload.js" charset="utf-8"></script>

		<script type="text/javascript" src="assets/js/bootstrap.js"></script>
	</body>
</html>
<style media="screen">
/*
/* Created by Filipe Pina
* Specific styles of signin, register, component
*/
/*
* General styles
*/

body, html{
   height: 100%;
background-repeat: no-repeat;
background-image: url('localizar.jpg');
background-size: 100% 100%;
font-family: 'Oxygen', sans-serif;
}

.main{
margin-top: 70px;
}

h1.title {
font-size: 50px;
font-family: 'Passion One', cursive;
font-weight: 400;
}

hr{
width: 10%;
color: #fff;
}

.form-group{
margin-bottom: 15px;
}

label{
margin-bottom: 15px;
}

input,
input::-webkit-input-placeholder {
  font-size: 11px;
  padding-top: 3px;
}

.main-login{
background-color: #fff;
  /* shadows and rounded borders */
  -moz-border-radius: 2px;
  -webkit-border-radius: 2px;
  border-radius: 2px;
  -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
  -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);

}

.main-center{
margin-top: 30px;
margin: 0 auto;
max-width: 330px;
  padding: 40px 40px;

}

.login-button{
margin-top: 5px;
}

.login-register{
font-size: 11px;
text-align: center;
}

</style>
